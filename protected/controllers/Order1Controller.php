<?php

class Order1Controller extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','delete','laporana','laporanb','laporanc'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Order1;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Order1']))
		{
			$model->attributes=$_POST['Order1'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->Number));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Order1']))
		{
			$model->attributes=$_POST['Order1'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->Number));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Order1');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Order1('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Order1']))
			$model->attributes=$_GET['Order1'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Order1 the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Order1::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Order1 $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='order1-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	public function actionLaporana()
	{
		$sql="select sales_person.Name, order1.Cust_ID
				from sales_person, customer, order1
				where order1.ID_Sales_Person=sales_person.ID_Sales_Person 
				and order1.Cust_ID=customer.Cust_ID 
				and order1.Cust_ID=4";
		$cmd= Yii::app()->db->createCommand($sql);
		$rows=$cmd->queryAll();
		$this->render('laporana',array('rows'=>$rows,));
	}
	
	public function actionLaporanb()
	{
		$sql="select sales_person.Name, order1.Cust_ID
				from sales_person, customer, order1
				where order1.ID_Sales_Person=sales_person.ID_Sales_Person 
				and order1.Cust_ID=customer.Cust_ID 
				and order1.Cust_ID != 4";
		$cmd= Yii::app()->db->createCommand($sql);
		$rows=$cmd->queryAll();
		$this->render('laporanb',array('rows'=>$rows,));
	}
	
	public function actionLaporanc()
	{
		$sql="SELECT sales_person.Name, order1.ID_Sales_Person
                from sales_person, order1
                WHERE sales_person.ID_Sales_Person=order1.ID_Sales_Person
                GROUP BY order1.ID_Sales_Person HAVING (COUNT(order1.ID_Sales_Person)>2)";
		$cmd= Yii::app()->db->createCommand($sql);
		$rows=$cmd->queryAll();
		$this->render('laporanc',array('rows'=>$rows,));
	}
}