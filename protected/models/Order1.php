<?php

/**
 * This is the model class for table "order1".
 *
 * The followings are the available columns in table 'order1':
 * @property integer $Number
 * @property string $Order_Date
 * @property integer $Cust_ID
 * @property integer $ID_Sales_Person
 * @property integer $Amount
 *
 * The followings are the available model relations:
 * @property Customer $cust
 * @property SalesPerson $iDSalesPerson
 */
class Order1 extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'order1';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Order_Date, Cust_ID, ID_Sales_Person, Amount', 'required'),
			array('Cust_ID, ID_Sales_Person, Amount', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('Number, Order_Date, Cust_ID, ID_Sales_Person, Amount', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cust' => array(self::BELONGS_TO, 'Customer', 'Cust_ID'),
			'iDSalesPerson' => array(self::BELONGS_TO, 'SalesPerson', 'ID_Sales_Person'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Number' => 'Number',
			'Order_Date' => 'Order Date',
			'Cust_ID' => 'Cust',
			'ID_Sales_Person' => 'Id Sales Person',
			'Amount' => 'Amount',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Number',$this->Number);
		$criteria->compare('Order_Date',$this->Order_Date,true);
		$criteria->compare('Cust_ID',$this->Cust_ID);
		$criteria->compare('ID_Sales_Person',$this->ID_Sales_Person);
		$criteria->compare('Amount',$this->Amount);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Order1 the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
