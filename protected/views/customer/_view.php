<?php
/* @var $this CustomerController */
/* @var $data Customer */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('Cust_ID')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->Cust_ID), array('view', 'id'=>$data->Cust_ID)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Name')); ?>:</b>
	<?php echo CHtml::encode($data->Name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('City')); ?>:</b>
	<?php echo CHtml::encode($data->City); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Industry_Type')); ?>:</b>
	<?php echo CHtml::encode($data->Industry_Type); ?>
	<br />


</div>