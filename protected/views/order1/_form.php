<?php
/* @var $this Order1Controller */
/* @var $model Order1 */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'order1-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'Order_Date'); ?>
		<?php echo $form->textField($model,'Order_Date'); ?>
		<?php echo $form->error($model,'Order_Date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Cust_ID'); ?>
		<?php echo $form->textField($model,'Cust_ID'); ?>
		<?php echo $form->error($model,'Cust_ID'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ID_Sales_Person'); ?>
		<?php echo $form->textField($model,'ID_Sales_Person'); ?>
		<?php echo $form->error($model,'ID_Sales_Person'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Amount'); ?>
		<?php echo $form->textField($model,'Amount'); ?>
		<?php echo $form->error($model,'Amount'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->