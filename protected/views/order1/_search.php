<?php
/* @var $this Order1Controller */
/* @var $model Order1 */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'Number'); ?>
		<?php echo $form->textField($model,'Number'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Order_Date'); ?>
		<?php echo $form->textField($model,'Order_Date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Cust_ID'); ?>
		<?php echo $form->textField($model,'Cust_ID'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ID_Sales_Person'); ?>
		<?php echo $form->textField($model,'ID_Sales_Person'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Amount'); ?>
		<?php echo $form->textField($model,'Amount'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->