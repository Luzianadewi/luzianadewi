<?php
/* @var $this Order1Controller */
/* @var $data Order1 */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('Number')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->Number), array('view', 'id'=>$data->Number)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Order_Date')); ?>:</b>
	<?php echo CHtml::encode($data->Order_Date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Cust_ID')); ?>:</b>
	<?php echo CHtml::encode($data->Cust_ID); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID_Sales_Person')); ?>:</b>
	<?php echo CHtml::encode($data->ID_Sales_Person); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Amount')); ?>:</b>
	<?php echo CHtml::encode($data->Amount); ?>
	<br />


</div>