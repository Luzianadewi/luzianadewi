<?php
/* @var $this Order1Controller */
/* @var $model Order1 */

$this->breadcrumbs=array(
	'Order1s'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Order1', 'url'=>array('index')),
	array('label'=>'Manage Order1', 'url'=>array('admin')),
);
?>

<h1>Create Order1</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>