<?php
/* @var $this Order1Controller */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Order1s',
);

$this->menu=array(
	array('label'=>'Create Order1', 'url'=>array('create')),
	array('label'=>'Manage Order1', 'url'=>array('admin')),
);
?>

<h1>Order1s</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
