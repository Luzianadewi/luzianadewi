<?php
/* @var $this Order1Controller */
/* @var $model Order1 */

$this->breadcrumbs=array(
	'Order1s'=>array('index'),
	$model->Number=>array('view','id'=>$model->Number),
	'Update',
);

$this->menu=array(
	array('label'=>'List Order1', 'url'=>array('index')),
	array('label'=>'Create Order1', 'url'=>array('create')),
	array('label'=>'View Order1', 'url'=>array('view', 'id'=>$model->Number)),
	array('label'=>'Manage Order1', 'url'=>array('admin')),
);
?>

<h1>Update Order1 <?php echo $model->Number; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>