<?php
/* @var $this Order1Controller */
/* @var $model Order1 */

$this->breadcrumbs=array(
	'Order1s'=>array('index'),
	$model->Number,
);

$this->menu=array(
	array('label'=>'List Order1', 'url'=>array('index')),
	array('label'=>'Create Order1', 'url'=>array('create')),
	array('label'=>'Update Order1', 'url'=>array('update', 'id'=>$model->Number)),
	array('label'=>'Delete Order1', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->Number),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Order1', 'url'=>array('admin')),
);
?>

<h1>View Order1 #<?php echo $model->Number; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'Number',
		'Order_Date',
		'Cust_ID',
		'ID_Sales_Person',
		'Amount',
	),
)); ?>
