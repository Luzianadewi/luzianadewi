<?php
/* @var $this SalesPersonController */
/* @var $data SalesPerson */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID_Sales_Person')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID_Sales_Person), array('view', 'id'=>$data->ID_Sales_Person)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Name')); ?>:</b>
	<?php echo CHtml::encode($data->Name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Age')); ?>:</b>
	<?php echo CHtml::encode($data->Age); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Salary')); ?>:</b>
	<?php echo CHtml::encode($data->Salary); ?>
	<br />


</div>