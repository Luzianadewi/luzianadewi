<?php
/* @var $this SalesPersonController */
/* @var $model SalesPerson */

$this->breadcrumbs=array(
	'Sales People'=>array('index'),
	$model->Name=>array('view','id'=>$model->ID_Sales_Person),
	'Update',
);

$this->menu=array(
	array('label'=>'List SalesPerson', 'url'=>array('index')),
	array('label'=>'Create SalesPerson', 'url'=>array('create')),
	array('label'=>'View SalesPerson', 'url'=>array('view', 'id'=>$model->ID_Sales_Person)),
	array('label'=>'Manage SalesPerson', 'url'=>array('admin')),
);
?>

<h1>Update SalesPerson <?php echo $model->ID_Sales_Person; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>